package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import view.InterfaceGraficaInicial;

public class AcoesInterfaceCadastro implements ActionListener {

	private JPanel painelCadastro;
	private JFrame telaInicial;
	private JTextField nomeEntrada;
	private JTextField anoEntrada;
	private JTextField generoEntrada;
	
	public AcoesInterfaceCadastro(JPanel painelCadastro,JFrame telaInicial,JTextField nomeEntrada,JTextField anoEntrada,JTextField generoEntrada) throws IOException{
		this.painelCadastro=painelCadastro;
		this.telaInicial=telaInicial;
		this.nomeEntrada=nomeEntrada;
		this.anoEntrada=anoEntrada;
		this.generoEntrada=generoEntrada;
		
		
	}
	
	
	public void salvarArquivo(String nome,String ano,String genero) throws IOException{
		PrintWriter gravarArq = new PrintWriter(new FileWriter("Arquivos/filmes.txt",true));
		 
			
			gravarArq.println(nome);
			gravarArq.println(ano);
			gravarArq.println(genero);
			
			gravarArq.close();
		
	    	 
	    
	  
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		
		String comando = e.getActionCommand();
		
		if(comando.equals("Ok")){
			
			if(nomeEntrada.getText().equals("")||generoEntrada.getText().equals("")||anoEntrada.getText().equals("")){
				JOptionPane.showMessageDialog(null,"Algum campo está em branco!");
				
				
			}
			else{
				try {
					
					salvarArquivo(nomeEntrada.getText(),anoEntrada.getText(),generoEntrada.getText());
					
					
				} catch (IOException e1) {
					System.out.println("Arquivo não encontrado!");
				}
				JOptionPane.showMessageDialog(null,"Cadastro realizado com sucesso!");
				painelCadastro.setVisible(false);
				try {
					new InterfaceGraficaInicial(telaInicial);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		    
			
		}
		else if(comando.equals("Cancelar")){
			nomeEntrada.setText("");
			anoEntrada.setText("");
			generoEntrada.setText("");
			
			
		}
		else{
			painelCadastro.setVisible(false);
			try {
				new InterfaceGraficaInicial(telaInicial);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}

	
	
}
