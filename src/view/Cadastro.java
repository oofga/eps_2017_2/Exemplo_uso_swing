package view;

import java.awt.Font;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controllers.AcoesInterfaceCadastro;

public class Cadastro {
	private JPanel painelCadastro;
	
	public Cadastro(JFrame telaInicial) throws IOException{
		
		criaPainelCadastros(telaInicial);
	}

	private void criaPainelCadastros(JFrame telaInicial) throws IOException {
		
		JLabel nome = new JLabel("Insira o nome do filme");
		JTextField nomeEntrada = new JTextField();
		JLabel ano = new JLabel("Insira o ano de lançamento do filme");
		JTextField anoEntrada = new JTextField();
		JLabel genero = new JLabel("Insira o gênero do filme");
		JTextField generoEntrada = new JTextField();
		JLabel titulo = new JLabel("Cadastro");
		JButton ok = new JButton("Ok");
		JButton cancelar = new JButton("Cancelar");
		JButton voltar = new JButton("Voltar");
		
		titulo.setBounds(450,100,200,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		ok.setBounds(345,330,150,30);
		cancelar.setBounds(505,330,150,30);
		voltar.setBounds(345,370, 310, 30);
		
		nome.setBounds(345,150,200,20);
		nomeEntrada.setBounds(345, 180, 310, 20);
		
		ano.setBounds(345,210,300,20);
		anoEntrada.setBounds(345, 240, 310, 20);

		
		genero.setBounds(345,270,200,20);
		generoEntrada.setBounds(345,300, 310, 20);
		
		
		
		painelCadastro = new JPanel();
		painelCadastro.setLayout(null);
		
		ok.setActionCommand("Ok");
		cancelar.setActionCommand("Cancelar");
		
		ok.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,anoEntrada,generoEntrada));
		cancelar.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,anoEntrada,generoEntrada));
		voltar.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,anoEntrada,generoEntrada));
		
		painelCadastro.add(ok);
		painelCadastro.add(cancelar);
		painelCadastro.add(voltar);
		painelCadastro.add(nome);
		painelCadastro.add(nomeEntrada);
		painelCadastro.add(ano);
		painelCadastro.add(anoEntrada);
		painelCadastro.add(genero);
		painelCadastro.add(generoEntrada);
		
		telaInicial.add(painelCadastro);
		telaInicial.setVisible(true);
		painelCadastro.setVisible(true);
	}
	
	
}

